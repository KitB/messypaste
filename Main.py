#!/usr/bin/env python2
# Copyright (c) 2012, Kit Barnes
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import inspect
import logging

import web
import markdown

from google.appengine.ext import db
from google.appengine.api import users

import mdx_mathjax
from Form import Form, CombinedForm

def md(text):
    return markdown.markdown(text, ['codehilite(force_linenos=True)', 'fenced_code', 'wikilinks(end_url=)', 'mathjax', 'toc'])

def getLoginURL():
    return users.create_login_url(web.ctx.get('path', '/'))

def ensureLogin():
    user = users.get_current_user()
    if user:
        return user
    else:
        raise web.seeother(getLoginURL())

def ensureAdmin():
    user = ensureLogin()
    if users.is_current_user_admin():
        return user
    else:
        raise web.seeother("/")

class Mess(db.Model):
    outString = db.TextProperty()

class Link(db.Model):
    desc = db.StringProperty(required=True)
    href = db.StringProperty(required=True)

    @staticmethod
    def getAll():
        q = db.Query(Link)
        return q

def getForm():
    form = Form( web.form.Textarea( name='outString', description="Mess:", rows="30" )
                        , web.form.Button( 'Save Mess', class_="btn btn-mini btn-primary" )
                        )
    return form

render = web.template.render('templates/', base='base', globals={'getAllLinks':Link.getAll})

class index:
    def GET(self):
        return render.mess("index", None, "Add /something to the url")

class mess:
    form = getForm()
    def GET(self, name):
        form = self.form()
        name = name.lower().rstrip('/')
        m = db.get(db.Key.from_path('Mess', name))
        outString = md(m.outString) if m else ''
        return render.mess(name, form, outString)

    def POST(self, name):
        form = self.form()
        name = name.lower().rstrip('/')
        if not form.validates():
            return "No validate"
        m = Mess( key_name=name
                , urlString=name
                , outString=form.d.outString
                )
        m.put()
        raise web.seeother("/%s" % name)

class edit:
    form = getForm()
    def GET(self, name):
        ensureAdmin() # You're damn right *I* can edit them
        form = self.form()
        name = name.lower()
        m = db.get(db.Key.from_path('Mess', name))
        outString = m.outString if m else ''
        form.fill( outString=outString )
        return render.mess(name, form, '')

    def POST(self, name):
        ensureAdmin()
        form = self.form()
        name = name.lower()
        if not form.validates():
            return "No validate"
        m = Mess( key_name=name
                , urlString=name
                , outString=form.d.outString
                )
        m.put()
        raise web.seeother("/%s/edit" % name)

class code:
    def GET(self):
        t = inspect.getsource(inspect.getmodule(self.__class__))
        t = "## Main source\n~~~~{.python}\n"+t+"\n~~~~"
        t += "\nFind the full source on [Bitbucket](https://bitbucket.org/KitB/messypaste)"
        return render.mess("code", None, md(t))

class admin:
    nlf = Form( web.form.Textbox(description="Description", name="desc"), web.form.Textbox("href"), web.form.Button("Add Link") )

    def GET(self):
        user = ensureAdmin()
        newLinkForm = CombinedForm( [self.nlf()], action="", method="post" )
        return render.admin(newLinkForm)

    def POST(self):
        user = ensureAdmin()
        f = self.nlf()
        if not f.validates():
            return "Form didn't validate"
        l = Link(**f.d)
        l.put()
        web.seeother("/admin")

class renderMD:
    def POST(self):
        data = web.input()
        markdown = data.markdown
        web.header("Cache-Control", "no-cache")
        return md(markdown)

urls = ( '/', index
       , '/code', code
       , '/source', code
       , '/admin', admin
       , '/(.*)/edit', edit
       , '/render', renderMD
       , '/(.*)', mess
       )

app = web.application(urls, globals())
application = app.wsgifunc()
