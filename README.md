# MessyPaste
MessyPaste is primarily a thing I threw together to get to grips with Google App Engine and web.py.

It's a write-once (like pastebin type sites) interlinkable (like wikis) url-keyed ... thingy.

It accepts markdown input and uses pygments to add syntax highlighting to code and MathJax to turn LaTeX formulae into display formulae.

Everything on mess.ninjalith.com is a mess except `/source` and `/code` which both display the currently running source code via python's inspect module.

# Running
To run you'll need:

* Google App Engine, Python SDK
* Python modules:
    - markdown
    - web
    - pygments

I personally had to make symlinks to the module folders inside the code folder for GAE to recognise the libraries. On the plus side appcfg.py will automatically traverse those symlinks for you so when you upload to GAE it doesn't bugger things up.

# TODO

* Use [CodeMirror](http://codemirror.net/) to provide a slick markdown input interface.
* Allow for deletion or correction within a couple of minutes of posting a mess in case of mistakes.
* Improve aesthetics
