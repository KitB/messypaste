timeout = 500;
function previewMD(){
    console.log("Getting preview from server")
    txt = editor.getValue();
    $("#md-preview").load("/render", {markdown:txt}, mathIt);
}
function mathIt(){
    var math = document.getElementById("md-preview")
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, math]);
}
var editor = CodeMirror.fromTextArea(document.getElementById("outString"), {
    mode: 'markdown',
    lineNumbers: true,
    matchBrackets: true,
    theme: "monokai",
    autofocus: true,
    onChange: actionWithDelay(previewMD)
});
function actionWithDelay(action){
    var callcount = 0;
    var delayAction = function(action, time){
        var expectcallcount = callcount;
        var delay = function(){
            if(callcount == expectcallcount){
                action();
            }
        }
        setTimeout(delay, time);
    }
    return function(eventtrigger){
        ++callcount;
        delayAction(action, timeout);
    }
}
previewMD()
