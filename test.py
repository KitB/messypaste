import markdown
t = """    #!/usr/bin/env python2
    import cgi
    import datetime
    import web
    import markdown
    from google.appengine.ext import db
    from google.appengine.ext.webapp.util import run_wsgi_app
    class Mess(db.Model):
            urlString = db.StringProperty()
            outString = db.TextProperty()
        
        render = web.template.render('templates/', base='base')
        
        class index:
                form = web.form.Form( web.form.Textarea( name='outString', description="Mess:" )
                                    , web.form.Button( 'Save Mess' )
                                    )
                def GET(self, name):
                        form = self.form()
                        m = db.get(db.Key.from_path('Mess', name))
                        outString = markdown.markdown(m.outString) if m else ''
                        return render.mess(name, form, outString)
            
                def POST(self, name):
                        form = self.form()
                        if not form.validates():
                                return "No validate"
                        m = Mess( key_name=name
                                , urlString=name
                                , outString=form.d.outString
                                )
                        m.put()
                        raise web.seeother("/%s" % name)
            
            urls = ( '/(.*)', index
                   )
            
            app = web.application(urls, globals())
            application = app.wsgifunc()"""
print markdown.markdown(t, ['codehilite'])
